<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Reason_WP_Test
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'reason-wp-test' ); ?></a>

	<header id="masthead" class="site-header header">
    <div class="header__top">
      <div class="search">
        <button class="search-button"><SearchIcon w="20px" h="20px" />Search</button>
        <!-- I assume some popup search thing will be here -->
      </div>
	  <!-- These would typically be defined as a custom menu area like the main nav -->
      <div class="top-links">
        <ul class="top-links__list">
          <li class="top-links__item"><a href="/">Learn</a></li>
          <li class="top-links__item _pink"><a href="/">Donate</a></li>
        </ul>
      </div>
    </div>
	<?php
		the_custom_logo();
		if ( is_front_page() && is_home() ) :
			?>
			<h1 class="site-title">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img class="logo" alt="<?php bloginfo( 'name' ); ?>" src="<?= get_template_directory_uri() . "/assets/logo.png"?>" />
				</a>
			</h1>
			<?php
		else :
			?>
			<p class="site-title">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
					<img class="logo" alt="<?php bloginfo( 'name' ); ?>" src="<?= get_template_directory_uri() . "/assets/logo.png"?>" />
				</a>
			</p>
			<?php
		endif;
	?>

	<nav class="navigation-bar">
	<?php
		wp_nav_menu( array( 
			'theme_location' => 'main-menu', 
			'container_class' => '' ) ); 
		?>
  </nav>
  </header><!-- #masthead -->
