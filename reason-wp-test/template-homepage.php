<?php /* Template Name: Home Style Template */ get_header(); ?>

	<main id="primary" class="site-main">

    <!-- CMS content -->
    <section class=""><?php the_content(); ?></section>
    <?php get_template_part('template-parts/blocks/posts/latest'); ?>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
