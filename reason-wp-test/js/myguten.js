wp.domReady( function() {
    // Add and remove block styles from the editor here -cp

    wp.blocks.registerBlockStyle( 'core/cover', {
        name: 'offset-background',
        label: 'Offset Background'
    } );

    wp.blocks.registerBlockStyle( 'core/button', {
        name: 'outline',
        label: 'Outline'
    } );

    wp.blocks.registerBlockStyle( 'core/column', {
        name: 'v-align',
        label: 'Vertically Aligned'
    } );

    
    wp.blocks.registerBlockStyle( 'core/column', {
        name: 'bg-img',
        label: 'Half Cover Background'
    } );
    

    

    // To see options in console, uncomment this:
    wp.blocks.getBlockTypes().forEach((block) => {
        if (_.isArray(block['styles'])) {
            console.log(block.name, _.pluck(block['styles'], 'name'));
        }
    });

} );




