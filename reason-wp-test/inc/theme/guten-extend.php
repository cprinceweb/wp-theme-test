<?php
function myguten_enqueue() {
    wp_enqueue_script(
        'myguten-script',
        get_template_directory_uri() . '/js/myguten.js',
        array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' )
//        filemtime( get_template_directory_uri() . '/js/myguten.js' )
    );
}
add_action( 'enqueue_block_editor_assets', 'myguten_enqueue' );