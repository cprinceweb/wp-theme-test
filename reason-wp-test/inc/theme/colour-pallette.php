<?php
/**
 * Theme Setup
 *
 */
function ea_setup()
{
    // Disable Custom Colors
    add_theme_support('disable-custom-colors');

    // Editor Color Palette
    add_theme_support('editor-color-palette', array(
        array(
            'name' => __('Blue', 'ea-starter'),
            'slug' => 'blue',
            'color' => '#477a8d',
        ),
        array(
            'name' => __('Green', 'ea-starter'),
            'slug' => 'green',
            'color' => '#478175',
        ),

        array(
            'name' => __('Pink', 'ea-starter'),
            'slug' => 'pink',
            'color' => '#be354b',
        ),

        array(
            'name' => __('Black', 'ea-starter'),
            'slug' => 'black',
            'color' => '#121212',
        ),

        array(
            'name' => __('Grey', 'ea-starter'),
            'slug' => 'grey',
            'color' => '#ebebeb',
        ),

        array(
            'name' => __('White', 'ea-starter'),
            'slug' => 'white',
            'color' => '#FFFFFF',
        ),
    ));
}

add_action('after_setup_theme', 'ea_setup');