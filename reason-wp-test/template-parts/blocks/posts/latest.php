<?php 
   // the query
   $the_query = new WP_Query( array(
      'post_status' => 'publish',
      'posts_per_page' => 4,
      'order'=>'DESC',
      'orderby'=>'ID',
   )); 
?>

<?php if ( $the_query->have_posts() ) : ?>
    
<section class="post-list">
    <h2>What do we do?</h2>
    <p>You might not have heard of us, but we're the people behind the following impactful programmes.</p>
    <dl class="post-list__items">
    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <di class="post-list__item">
            <dt><?php the_title(); ?></dt>
            <dd>
                <p><?php the_excerpt(); ?></p>
                <a class="action primary outline" href="<?= the_permalink(); ?>">Learn More</a>
            </dd>
        </di>	

    <?php endwhile; ?>
    </dl>
  <?php wp_reset_postdata(); ?>

<?php else : ?>
  <p><?php __('No News'); ?></p>
<?php endif; ?>
    <div class="actions">
        <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="action primary">More about what we do</a>
    </div>
</section>